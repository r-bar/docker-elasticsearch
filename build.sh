#!/bin/sh

docker build . -t docker-elasticsearch

while read tag; do
  docker tag docker-elasticsearch registry.gitlab.com/r-bar/docker-elasticsearch:$tag
done < docker-tags.txt
