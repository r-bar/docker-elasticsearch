FROM docker.elastic.co/elasticsearch/elasticsearch:6.6.0
RUN ./bin/elasticsearch-plugin install -b analysis-icu
RUN ./bin/elasticsearch-plugin install -b ingest-attachment
RUN ./bin/elasticsearch-plugin install -b repository-s3

