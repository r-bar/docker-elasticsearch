# Docker Elasticsearch (batteries included)

## Building
Add any desired tags to `docker-tags.txt`. Then run the following to build and
tag:
```
sh build.sh
```

## Publishing
```
sh push.sh
```
